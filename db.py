#-*- coding: utf-8 -*-
from orator import DatabaseManager, Model
import os
# Aqui va la configuracion mysql
databases = {
    'mysql': {
        'driver': 'mysql',
        'host': 'localhost',
        'database': 'id',
        'user': 'test',
        'password': 'test1234',
        'prefix': ''
    }
}

db = DatabaseManager(databases)
Model.set_connection_resolver(db)